resource "azurerm_resource_group" "custom_rg" {
  name     = "Custom-Resource-Group"
  location = var.default_location
  tags = {
    Onwer = "Test"
  }
}

resource "azurerm_virtual_network" "main" {
  name                = "${var.prefix}-network"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.custom_rg.location
  resource_group_name = azurerm_resource_group.custom_rg.name
}

resource "azurerm_subnet" "internal" {
  name                 = "internal"
  resource_group_name  = azurerm_resource_group.custom_rg.name
  virtual_network_name = azurerm_virtual_network.main.name
  address_prefixes     = ["10.0.2.0/24"]
}

resource "azurerm_public_ip" "unix" {
  name                = "unix-ip"
  resource_group_name = azurerm_resource_group.custom_rg.name
  location            = azurerm_resource_group.custom_rg.location
  allocation_method   = "Static"

  tags = {
    environment = "Production-Unix-VM"
  }
}

resource "azurerm_public_ip" "win" {
  name                = "win-ip"
  resource_group_name = azurerm_resource_group.custom_rg.name
  location            = azurerm_resource_group.custom_rg.location
  allocation_method   = "Static"

  tags = {
    environment = "Production-Win-VM"
  }
}

resource "azurerm_network_interface" "main" {
  name                = "${var.prefix}-nic-linux"
  location            = azurerm_resource_group.custom_rg.location
  resource_group_name = azurerm_resource_group.custom_rg.name

  ip_configuration {
    name                          = "testconfiguration1"
    subnet_id                     = azurerm_subnet.internal.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.unix.id
  }
}

resource "azurerm_network_interface" "win_main" {
  name                = "${var.prefix}-nic-win"
  location            = azurerm_resource_group.custom_rg.location
  resource_group_name = azurerm_resource_group.custom_rg.name

  ip_configuration {
    name                          = "testconfiguration2"
    subnet_id                     = azurerm_subnet.internal.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.win.id
  }
}

resource "azurerm_network_security_group" "main" {
  name                = "customNetworkSecurityGroup"
  location            = azurerm_resource_group.custom_rg.location
  resource_group_name = azurerm_resource_group.custom_rg.name

  security_rule {
    name                       = "Custom-Linux-Rule"
    priority                   = 1001
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "Allow-Ping-Rule"
    priority                   = 1002
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Icmp"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  tags = {
    environment = "Terraform Demo"
  }
}

resource "azurerm_network_security_group" "custom_win_group" {
  name                = "customNetworkSecurityGroup-Win"
  location            = azurerm_resource_group.custom_rg.location
  resource_group_name = azurerm_resource_group.custom_rg.name

  security_rule {
    name                       = "Custom-Windows-Rule"
    priority                   = 1001
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_ranges    = [5985, 22, 3389]
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "Allow-Ping-Rule"
    priority                   = 1002
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Icmp"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  tags = {
    environment = "Terraform Demo"
  }
}

# Connect the security group to the network interface
resource "azurerm_network_interface_security_group_association" "unix" {
  network_interface_id      = azurerm_network_interface.main.id
  network_security_group_id = azurerm_network_security_group.main.id
}

# Connect the security group to the network interface
resource "azurerm_network_interface_security_group_association" "win" {
  network_interface_id      = azurerm_network_interface.win_main.id
  network_security_group_id = azurerm_network_security_group.custom_win_group.id
}


resource "azurerm_linux_virtual_machine" "main" {
  name                  = "${var.prefix}-unix-vm"
  location              = azurerm_resource_group.custom_rg.location
  resource_group_name   = azurerm_resource_group.custom_rg.name
  network_interface_ids = [azurerm_network_interface.main.id]
  size                  = "Standard_B1s"

  computer_name   = "azure-lab-1"
  admin_username  = "ubuntu"
  admin_password  = "P@$$w0rd1234!"
  disable_password_authentication = false

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }
  os_disk {
    name                 = "myosdisk1"
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }
  tags = {
    environment = "staging"
  }
}

# resource "azurerm_windows_virtual_machine" "main" {
#   name                = "${var.prefix}-win-vm"
#   resource_group_name = azurerm_resource_group.custom_rg.name
#   location            = azurerm_resource_group.custom_rg.location
#   size                = "Standard_F2"
#   admin_username      = "adminuser"
#   admin_password      = var.windows_passwd
#   provision_vm_agent  = true
#   network_interface_ids = [
#     azurerm_network_interface.win_main.id,
#   ]

#   os_disk {
#     caching              = "ReadWrite"
#     storage_account_type = "Standard_LRS"
#   }

#   source_image_reference {
#     publisher = "MicrosoftWindowsServer"
#     offer     = "WindowsServer"
#     sku       = "2016-Datacenter"
#     version   = "latest"
#   }
# }

#create the actual VM
resource "azurerm_virtual_machine" "win" {
  name                = "${var.prefix}-win-vm"
  resource_group_name = azurerm_resource_group.custom_rg.name
  location            = azurerm_resource_group.custom_rg.location
  network_interface_ids = [
    azurerm_network_interface.win_main.id,
  ]
  vm_size = "Standard_F2"

  storage_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = "2016-Datacenter"
    version   = "latest"
  }

  storage_os_disk {
    name              = "osdisk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name  = "${var.prefix}-win-vm"
    admin_username = "adminuser"
    admin_password = var.windows_passwd
    custom_data    = file("./files/winrm.ps1")
  }

  os_profile_windows_config {
    provision_vm_agent = true
    winrm {
      protocol = "http"
    }
    # Auto-Login's required to configure WinRM
    additional_unattend_config {
      pass         = "oobeSystem"
      component    = "Microsoft-Windows-Shell-Setup"
      setting_name = "AutoLogon"
      content      = "<AutoLogon><Password><Value>${var.windows_passwd}</Value></Password><Enabled>true</Enabled><LogonCount>1</LogonCount><Username>adminuser</Username></AutoLogon>"
    }

    # Unattend config is to enable basic auth in WinRM, required for the provisioner stage.
    additional_unattend_config {
      pass         = "oobeSystem"
      component    = "Microsoft-Windows-Shell-Setup"
      setting_name = "FirstLogonCommands"
      content      = file("./files/FirstLogonCommands.xml")
    }
  }

  connection {
    host     = azurerm_public_ip.win.ip_address
    type     = "winrm"
    port     = 5985
    https    = false
    timeout  = "10m"
    user     = "adminuser"
    password = var.windows_passwd
  }

  provisioner "file" {
    source      = "files/config.ps1"
    destination = "c:/terraform/config.ps1"
  }

  provisioner "remote-exec" {
    on_failure = continue
    inline = [
      "powershell.exe -ExecutionPolicy Bypass -File C:/terraform/config.ps1",
    ]
  }
  provisioner "local-exec" {
    command = "terraform output -json > ./ansible/data/ip.json; export ANSIBLE_HOST_KEY_CHECKING=False ;ansible-playbook -i ./ansible/hosts ./ansible/site.yml"
  }

}

output "public_ip_address_unix" {
  value = azurerm_public_ip.unix.ip_address
}

output "public_ip_address_win" {
  value = azurerm_public_ip.win.ip_address
}
