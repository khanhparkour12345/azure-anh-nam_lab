
provider "azurerm" {
  version = "~>2.0"
  features {}
}

variable "default_location" {
  type = string
}

variable "prefix" {
  type = string
}

variable "ssh_pub_key_path" {
  type = string
}

variable "windows_passwd" {
  type = string
}
